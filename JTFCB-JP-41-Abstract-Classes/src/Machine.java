// abstrakcyjna klasa
public abstract class Machine {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// podobnie jak deklaracja metody w interface
	// narzuca zeby zrobic metody w dzieciecych klasach
	// klikasz errory
	// i masz je szybko
	// po to jest wlasnie to
	public abstract void start();
	public abstract void doStuff();
	public abstract void shutdown();
	
	
	public void run () {
		
		start();
		doStuff();
		shutdown();
		
	}
	
	
}
